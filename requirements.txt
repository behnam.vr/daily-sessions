numpy==1.24.2
PyMySQL==1.0.2
python-dotenv==0.21.1
matplotlib
jupyterlab
pandas