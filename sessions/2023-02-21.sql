CREATE TABLE Persons (
    PersonID int NOT NULL,
    LastName varchar(255) NOT NULL,
    FirstName varchar(255),
    Age int,
    PRIMARY KEY (PersonID)
);

-- DROP TABLE Persons;

CREATE TABLE Orders (
    OrderID int NOT NULL,
    OrderNumber int NOT NULL,
    PersonID int NOT NULL,
    PRIMARY KEY (OrderID),
    FOREIGN KEY (PersonID) REFERENCES Persons(PersonID)
);


INSERT INTO Persons (PersonID, LastName, FirstName, Age)
VALUES 
	(2, "Vakili", "Behnam", 31),
	(3, "Vakili", "Behnam", 31);

SELECT * FROM Persons;

INSERT INTO Orders (OrderID, OrderNumber, PersonID)
VALUES 
	(1, 100, 3),
	(2, 200, 2);


SELECT * FROM Orders;

