-- DROP TABLE Persons;


CREATE TABLE Couriers (
    CourierID int NOT NULL,
    LastName varchar(255) NOT NULL,
    PRIMARY KEY (CourierID)
);


CREATE TABLE Orders (
    OrderID int NOT NULL,
    CourierID int NOT NULL,
    PRIMARY KEY (OrderID),
    FOREIGN KEY (CourierID) REFERENCES Couriers(CourierID)
);


CREATE TABLE Products (
    ProductID int NOT NULL,
    Name varchar(255) NOT NULL,
    PRIMARY KEY (ProductID)
);


CREATE TABLE order_to_product_relationship (
	OrderID int NOT NULL,
	ProductID int NOT NULL,
    FOREIGN KEY (OrderID) REFERENCES Orders(OrderID),
    FOREIGN KEY (ProductID) REFERENCES Products(ProductID)
);



INSERT INTO Couriers (CourierID, LastName)
VALUES (2, "Emad");

INSERT INTO Products (ProductID, Name)
VALUES
	(1, "ghorme"),
	(2, "mirza ghasemi");

-- NEW ORDER
INSERT INTO Orders (OrderID, CourierID)
VALUES (1, 1);

-- based on number of products
INSERT INTO order_to_product_relationship (OrderID, ProductID)
VALUES 
	(1, 1),
	(1, 2);


select * 
from Couriers
where LastName = 'Ali'


-- SELECT 
-- 	o.OrderID,
-- 	c.LastName 
-- FROM 
-- 	Orders o
-- INNER JOIN Couriers c ON o.CourierID = c.CourierID 


SELECT
	o.OrderID,
	p.Name as "product name",
	c.LastName as "courier last name"
FROM Orders o 
INNER JOIN order_to_product_relationship otpr ON o.OrderID = otpr.OrderID
INNER JOIN Couriers c ON o.CourierID = c.CourierID
INNER JOIN Products p ON otpr.ProductID = p.ProductID


